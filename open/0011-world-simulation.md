- Feature Name: World Simulation
- Start Date: 2019-02-27
- Tracking Issue: https://gitlab.com/veloren/rfcs/issues/12

# Summary

[summary]: #summary

Veloren should have a concept for world simulation which scales well with servers / clients up to MMORPG style of players. Therefore a special arrangement of information, as well as granularity of information is needed.
This RFC explains the goals and the technical design of Veloren's World simulation.

# Motivation

[motivation]: #motivation

Imagine a simple Voxel game, storing RGB color information for every voxel.
The fact, that it has 3 dimensions, will let explore the amount of voxels quite quickly.
A simple world of 1000 blocks in every direction would make up 3 GB of data.

The concept of world simulation is to find a format that handles information to reduce memory consumption while keeping the performance high.
It should enable a client to quickly join a game and barely notice that information is compressed in the background.
It might even enable us, to split multiple regions to different threads or even different servers, so create gigantic worlds simulated on a scalable network of servers.

It should also address the problem of simulation other voxel games, like Minecraft have.
It only simulates loaded chunks, which can lead to some terrible transition regions between long simulated and new generated area.

World Simulation should cover the whole world state in one approach to deliver a immersive gaming experience.

Inpiratated by these 3 talks:
- https://www.youtube.com/watch?v=v2Q_zHG3vqg
- https://www.youtube.com/watch?v=JpmK0zu4Mts
- https://www.youtube.com/watch?v=kwnb9Clh2Is

# Lessons learned

we had multiple concepts how to do chunks and how to layout terrain data in the past.
(reminder: terrain data is not everything in world simulation)
We started with having chunks of a fixed size of 32 blocks. Ever block was a u8 meaning that a chunk was a array of 32x32x32 u8. All Chunks where in a Map and had a RwLock. This way when accessing multiple blocks we had to make a map lockup every access including taking and releasing a lock. The memory consumption was quite high, lets define it as 100%.

We improved this concept with compressed chunks, giving the option to not only store a full blown block (Hetero Chunk), but also a run-length-encoded version (RLE Chunk) which needed 2 u8 per block but could usually compress information to 40%.
Also we introduces homogeneous chunks (Homo Chunk) in cases all blocks where of the same type. This was mostly for underground or air chunks. This case only needed 0.0033%. In total we could archive like 50% Homo, 35% RLE and 15% Hetero chunks.

This system still had the disadvantage of locks and maps access, therefore we create the VolSample. A VolSample (aka. ChunkSample) holds a lock to a Chunk for a specific operation. This way we only needed a single Map lockup and lock for a whole chunk. We also speed up operations like a iterator over the whole chunk.

This system worked fine, as long as all accesses where coordinated, because a single lock to a chunk could halt other parts of the program. This is because we had to implement a try-lock function for chunk access.
Problematic where writing at different locations, because these introduce write-locks, which could possible halt the whole system.

# Dependency Job Model

Lets look at the graphic below, it contains a row per Thread and a Column per "time segment".
We have multiple tasks **physics**, **AI**, **network**, **render**. Those tasks can take up one or multiple time slots, we don't know before. At the beginning of a Frame, we schedule all tasks that needed to be done within it.
The threads take one of these tasks and calculate. Then when every task is done, there is a special sync period where write changes are done. After that sync period, the next Frame calculations can start.


|         | Frame1  |        |         |        | write sync period Frame1    | Frame2 |
|---------|---------|--------|---------|--------|-----------------------------|--------|
| Thread1 | physics | ai     | ai      | render | apply write changes phyiscs | ...    |
| Thread2 | phy     | sics   | network | render | apply write changes ai      | ...    |
| Thread3 | physics | render | render  |        | apply write changes network | ...    |
| Thread4 | physic  | s      | ai      | ai     |                             | ...    |

That has the advantage, that during a frame everything can be parallelized, we don't have writes to persistence in between. Only in the special designed sync period we need exclusive locks.
Obviously this creates some issues:

- The render of Frame1 can only render the information of Frame0, because the result of Frame1 does not exist yet, etc.
- Tasks need to be split-able for this to work, if a task is not split-able in multiple subtasks, we have a hard time scaling. Best is probably finding a attribute, like position to split tasks.
- Tasks must not depend on each other, tasks must create a result on their own and only depend on the information of the last frame calculation.
- The frame rate is somehow locked to the speed of ticks the client can be calculated

Okay, this way we must not have tasks that depend on each other, but it seems like a pretty hard restriction we want to loosen.
If we wouldn't loosen it, we might come to the problem that we need to merge 2 jobs into a single job and end up with a single super job, undergoing our job system.

Move data between jobs: We need some data containers we can fill in a job and then read in another job.
These containers can be read and write only by one job and then their ownership is transferred to another job (maybe it makes sense, to give the other job only read access and introduce a multi cast to transfer the read rights to multiple jobs)
Ideally this is done within the rust borrow model.

The most common type of container will probably be a vector, that is filled with data/ or commands for the next job.

To make sure that all containers a job need to run are already filled, we need to introduce a dependency model between jobs, it will be automatically deducted from the way containers are transferred.
The scheduler needs to calculate the order on how to trigger jobs.
Therefore it will calculate a list of jobs that are ready to run, jobs that are waiting(, finished jobs?).
A thread that ran a job successfully, can then request the next job from the scheduler.

Iterating jobs will be used as an optimization, they are basically a job, that will do an action for a iterator of objects.
This can be used to not overkill the job system with actually 100000 jobs, when we have that many entities, but have a single iterating job that will handle all 100000 entities. A batch-parameter will enable to be passed to the job in order to reduce function calls.
This is just an optimization measure.

###Example: Physics

Our physics system will need to check collisions between entities and terrain as well as entities and other entities.
it works by fist create boxes around these and then run a time to impact algorithm on these, compare it with the delta time and apply changes.

####Job1 - Iterate over all entities and generate the surrounding boxes.
Input:
1. Single ECS entity

Output:
1. array of Boxes

####Job2 - Calculate boxes from terrain around entity
Input:
1. entity
2. array of Boxes per entity

Output:
1. boxes of terrain around entity

####Job3 - Calculate TTI
Input:
1. entity
2. array of all boxes for all entities
3. array of all boxes of terrain around entity

Output:
1. TTI in for this entity

####Job4 - Calculate new position
Input:
1. entity
2. TTI

Output:
1. Action item to change position of entity at end of frame

Analysis:
Imagine 100 players and 400 NPCs as well as 100 flying entities and 100 monsters.
Lets say we generate a single box for the players and NPCs and flying entities this results in 600 boxes.
Lets say our monsters are a bit more complex and have 3 boxes around them. Total of 900 boxes.
The job#1 is called 700 times.
Now we generate boxes for all ground entities, because the ones flying will only have air around them (mostly).
Depending on the speed of the entities we need maybe 2 blocks around them.
This will result in (2+1+2) * (2+1+2) * (2+2+2) blocks per player and NPC, maybe (2+3+2) * (2+3+2) * (2+1+2) lookups per flying and (2+2+2) * (2+2+2) * (2+3+2) per monster.
Resulting in 75.000 + 24.500 + 25.200 = 124.700 lookups and around 75000 * 0.7 + 24500 * 0 + 25200 * 0.7 = 70.140
blocks and a total of 700 calls of job#2.
Job#3 is then called for every entity with the pre-calculated values 700 times.
In the end the last job of the physics system will calculate the new position of every entity and will create a action item for the frame end jobs which will actually modify the persistence state.

TODO: check if job#3 is executed multiple times, how to get dynamic iterations, jobs need to trigger other jobs

### Implementation in ECS

In order to have a entity component system to work with the read only jobs, we must make sure that no systems do actually Write to Components. All Systems need to write messages which in return are read by one giant end frame system and then applied to the components. It's a difficult task to keep the end frame system really fast because that will not scale with core count.


### Profiling

We can use Google Chromes Profiler because they have a json interface: https://docs.google.com/document/d/1CvAClvFfyA5R-PhYUmn5OOQtYMH4h6I0nSsKchNAySU/edit

# Region

We sperate out world into multiple regions, where every region must be able to simulate independent of other regions.
We use that fact, that everything in the world has coordinates and order the information according these attribute.
Therefore we divide the map into regions by 2048x2048x2048 blocks (size might vary or be variable).
Each region has it's own terrain, it's own ECS, all in all, it's own data.
This allows us, to parallel calculate separately regions or even calculate them on different host machines if needed.

As a consequence we have a service called the **region manager**.
It owns all regions and takes care of loading and saving them persistently.
When a server wants to join a cluster, it sends the region manager the amount of regions it can take over.
The region manager can than with the CPU data of the servers calculate which server takes what region.
servers send back their region state the region manager when saving periodically or when they shut down.

There is however a problem for everything with a width and height, these entries have the possibility to be in multiple regions at once.
Accessing data of another region directly introduces a direct dependency which needs to avoided at all costs.
Therefore we always have them owned by one region, but other regions might have a *reference copy* of them to locally work with these entries.
there *reference copy* will get updated to neighbour regions regularly.
These updates can happen be triggered during the work phase of a region and are received during the update phase of the receiver region.
The *reference copy* will then be handled by the region like it owned it, but it does never propagate its updates.
It's recommended to keep that zone as small as possible to reduce inconsistency, but have it and move entities to other regions after a small-time to reduce stress on the system if entities walk on the edge between 2 regions.
The default radius around a region should be 16 blocks deep.

This is achieved via a subscribe pattern. A subscriber might specify a area of a region to subscribe to.
It will then regularly get updates of the region.
This pattern can be used by clients and other regions as well.
In fact clients might just be a server which is not owning the area but simulating a *reference copy*.

# LOD Store

Inside a region we store information in different level of detail. The idea behind is that data needs to be somewhat compressed to handle it on a larger scale, we want to simulate everywhere, but we can allow abstracting a lot if nobody watches the area.
We introduce a LOD where regions with players get a fine wolrd simulation, regions further away get a more rought one.
We separate our data in multiple components like `temperature` and every gets it's own LOD.
It needs to be possible to upscale or downscale the component based on environmental data. This process can introduce loss, but loss should be always negligible.
The most detailed level is 0, the rougher a state, the higher the number.
The conversion from 0 -> 1 -> 0 might lose some details.
But conversion from 0 -> 1 -> 0 -> 1 -> 0 should not loose much more information.
We aim to write the same system (like physics) in multiple lods. It probably makes sense to write one physics system which requires level 0 access to terrain information.
However maybe one on level 2 makes sense with hightmap information as basis.
On Chunk or biome basis, physics doesn't make sense anymore.
Imagine a nature simulation, this should work from level 0 to level n up.
Some system might only depend on rough data, e.g. civilization data. While we prob dont care for the next building build in tier 0, but on a rought scale it's easier to make the decision which building makes most sense for a city.
Conclusion: We order information in components like temperature, terrain info, npc and add lod for each component.
The same logic/system is implemented for different lods.
When a specific system is requested, we make sure to automatically convert the component to the corresponding level.

# Region Simulation

This chapter aims to list detailed what the layout of all Data/ECS and Jobs are inside one single region.
It aims to give detailed information about every LOD for every componenent and how they are connected in a system to simulate the world at a specific level.

## Storage format

The LOD is combined with a Octtree.
All possible LOD combination cover a area of:
LOD n = $`2^n x 2^n x 2^n`$
blocks.
So the following are all possible whole block conbinations possible in a 2048 block region:

- LOD 0 = 1x1x1
- LOD 1 = 2x2x2
- LOD 2 = 4x4x4
- LOD 3 = 8x8x8
- LOD 4 = 16x16x16
- LOD 5 = 32x32x32
- LOD 6 = 64x64x64
- LOD 7 = 128x128x128
- LOD 8 = 256x256x256
- LOD 9 = 512x512x512
- LOD 10 = 1024x1024x1024
- LOD 11 = 2048x2048x2048

Additionally we have negative LOD levels to cover subblock information with a practical limit of -5:

- LOD -1 = $`1/2x1/2x1/2`$
- LOD -2 = $`1/4x1/4x1/4`$
- LOD -3 = $`1/8x1/8x1/8`$
- LOD -4 = $`1/16x1/16x1/16`$
- LOD -5 = $`1/32x1/32x1/32`$

## Optimization Chunks

Instead of providing an Octtree that needs to reach 11 steps for every block access, we provice a 32x32x32 stepping, where the following LODs are supported:
- 32x32x32 LOD -5 equals a LOD 0
- 32x32x32 LOD 0 equals a LOD 5
- 32x32x32 LOD 5 equals a LOD 10

TODO: how will this work, is it dependent of the type if this is supportet, maybe every type has their own type depending on the "next" LOD layer, e.g. LOD11 -> 8x8x8 LOD8 -> 8x8x8 LOD5 -> 32x32x32 LOD0 -> 32x32x32 LOD-5

TODO: how do LOD and ECS work together ? We dont want to put everything in "systems", that does not make sense in an ECS. Or is ECS just a part of one specific LOD if we have NPCs at entity level?

## Block type info

- LOD -5: Subblock details, flowers, in house stuff
- LOD 0: terrain blocks
- LOD 5: chunk info, number of stones, waterlevel, air amount etc.
- LOD 8: chunk info, number of stones, waterlevel, air amount etc.
- LOD 11: chunk info, number of stones, waterlevel, air amount etc.

## Temperature

- LOD -3: Subblock details
- LOD 0: per block
- LOD 5: per chunk
- LOD 11: region level

## Light

- LOD -3: Subblock details
- LOD 0: per block
- LOD 5: per chunk
- LOD 11: region level

## Evil

- LOD 2: 4x4x4
- LOD 5: per chunk
- LOD 11: region level

## Civilisation

- LOD 0: Per NPC information, position, inventory, velocity, skin, sex, color, weapon, armor, skills
- LOD 2: Per NPC: position, velocity, armor class, weapon class, skills, no visual stuff
- LOD 5: NPC groups: People in Group, weapons class total, armor class total, position
- LOD 8: Cities of people, count, rought information, number of smiths, number of woodworkers, ...
- LOD 11: even rougher City information

# Region Manager

This chapter describes in detail how the region manager will work.
There will be exactly one region manager per World.
The region manager is coordinating the servers without holding all the information.
Therefore its necessary that each server can reach the region manager and each server can reach each other server.

On initializing a size of the world is chosen, default 256x256x1 regions (roughtly the size of Great Britain)
The region manager will store region information in a Vec[0..256*256].
Each region gets an id from [-128,-128,0] to [127,127,0].
The third number is omitted, i16 should been used for the region id x and y coordinate (maximum size: 65536x65536 x1 regions, equal to 33 times the size of the earth)
Per region we hold a tick-time value, saying how long a average tick takes on this region and a serverid.

The serverid is translated to a Server in a Hashmap.
Per Server we hold the connection information (ip port).

The job of the region manager is to balance the regions to all servers in a way that:
- the total tick time is almost equal on all servers
- nearby regions are placed on the same server
- regions don't switch to often

## Protocol

A server A connects to the region manager to say that he wants to join the cluster.
Region manager answers with it's id, the global seed and a public RSA key. It sents an update with all server id's to all servers.

The manager will see that 0 regions have a server and new joined server A will take care of all of them.
The manager sends a package that server A will now handle all regions from [-128,-128] to [127,127], and that no old server exist for them and he is free to create those regions himself.

Server A will then start simulating the world on all these regions.
Server A will send a periodic update of the frametime needed to simulate these regions to the region manager.

Server B connects to the region manager and wants to join the cluster.
Region manager answes again with id, global seed, public RSA key and updates Server information with all.
The manage will see that server A has a higher ticktime than B, because B has no job yet. It will calculate and come to the conclusion that server A should handover all regions from [-128,-128] to [0,127] to server B.
So the manager sends and signed message to server B that he is allowed to take over regions from server A.

Server B will contact Server A with this message, Server A verifies it and sends Server B the serialized regions (directly, not over the region manager). Then A sends an update to the manager, that B now successfully got the regions [-128,-128] to [0,127].

Server B needs to subscribe to the edge of his regions, so he sends a request to the manager to tell the ID of the owner of the block [1,-128] to [1,127]. Server A does the same with region [0,-128] to [0,127]. After the manager answers, they both look up the ips and then connect to each other to subscribe to a part of that region.

Server A and B are sending periodically their tick times to the manager.
The manager periodically makes some adjustments to the region size according to tick times.

Server A wants to unregister, it sends it's wish to the region manager, which then marks the regions as unmaintained and transfers them to Server B to handle (like before).
After Server A has no responsible any more, it is safe to shut down completely.

##security

region manager needs to trust servers, that they keep the regions and don't act malicious. So a white-list might be needed to restrict access, maybe even a password.
For Handover of regions, the manager will create a signed message that allows a specific server to take a specific region at a specific time. Because servers cannot trust each others to really be official servers.
Servers can subscribe to each other without any additional security measurements.

## open questions

- how exactly will the subscribe pattern work, what happens when a server unregisters
- is security enough ? do we need to adjust it?
- can we have online resize of the whole world ? add reagions ?
- where does a client connect to ? the region manager ? one of the servers ? how does it get the crrect server info ? where are client information stored.

# Drawbacks

[drawbacks]: #drawbacks

- The region architecture requieres a pretty complicated framework for single host servers
- The region architecture might introduce smaller inaccurate result around the region boarder


# Rationale and alternatives

[alternatives]: #alternatives


# Unresolved questions

[unresolved]: #unresolved-questions
